<?php
/**
 * Created by PhpStorm.
 * User: bati
 * Date: 7/16/17
 * Time: 12:36 AM
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ClientRegisterController extends Controller
{
    /**
     * @Route("/register", name="registerpage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('client/register.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ));
    }

    /**
     * @Route("/client", name="registerclientpage")
     */
    public function registerClientAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('client/register.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ));
    }
}
