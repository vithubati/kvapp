<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientConfidential
 */
class ClientConfidential
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $maritalStatus;

    /**
     * @var string
     */
    private $placeOfMarriage;

    /**
     * @var \DateTime
     */
    private $dateOfMarriage;

    /**
     * @var string
     */
    private $immStatus;

    /**
     * @var \DateTime
     */
    private $dateOfArrival;

    /**
     * @var \DateTime
     */
    private $dateOfLanding;

    /**
     * @var \DateTime
     */
    private $citizenshipObtained;

    /**
     * @var string
     */
    private $immID;

    /**
     * @var string
     */
    private $fileNo;

    /**
     * @var string
     */
    private $refuCaseLawyer;

    /**
     * @var string
     */
    private $portOfEntry;

    /**
     * @var string
     */
    private $interpreterName;

    /**
     * @var string
     */
    private $referedBy;

    /**
     * @var string
     */
    private $homeTp;

    /**
     * @var string
     */
    private $workTp;

    /**
     * @var string
     */
    private $cellTp;

    /**
     * @var string
     */
    private $emer;

    /**
     * @var string
     */
    private $comments;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set maritalStatus
     *
     * @param string $maritalStatus
     * @return ClientConfidential
     */
    public function setMaritalStatus($maritalStatus)
    {
        $this->maritalStatus = $maritalStatus;

        return $this;
    }

    /**
     * Get maritalStatus
     *
     * @return string 
     */
    public function getMaritalStatus()
    {
        return $this->maritalStatus;
    }

    /**
     * Set placeOfMarriage
     *
     * @param string $placeOfMarriage
     * @return ClientConfidential
     */
    public function setPlaceOfMarriage($placeOfMarriage)
    {
        $this->placeOfMarriage = $placeOfMarriage;

        return $this;
    }

    /**
     * Get placeOfMarriage
     *
     * @return string 
     */
    public function getPlaceOfMarriage()
    {
        return $this->placeOfMarriage;
    }

    /**
     * Set dateOfMarriage
     *
     * @param \DateTime $dateOfMarriage
     * @return ClientConfidential
     */
    public function setDateOfMarriage($dateOfMarriage)
    {
        $this->dateOfMarriage = $dateOfMarriage;

        return $this;
    }

    /**
     * Get dateOfMarriage
     *
     * @return \DateTime 
     */
    public function getDateOfMarriage()
    {
        return $this->dateOfMarriage;
    }

    /**
     * Set immStatus
     *
     * @param string $immStatus
     * @return ClientConfidential
     */
    public function setImmStatus($immStatus)
    {
        $this->immStatus = $immStatus;

        return $this;
    }

    /**
     * Get immStatus
     *
     * @return string 
     */
    public function getImmStatus()
    {
        return $this->immStatus;
    }

    /**
     * Set dateOfArrival
     *
     * @param \DateTime $dateOfArrival
     * @return ClientConfidential
     */
    public function setDateOfArrival($dateOfArrival)
    {
        $this->dateOfArrival = $dateOfArrival;

        return $this;
    }

    /**
     * Get dateOfArrival
     *
     * @return \DateTime 
     */
    public function getDateOfArrival()
    {
        return $this->dateOfArrival;
    }

    /**
     * Set dateOfLanding
     *
     * @param \DateTime $dateOfLanding
     * @return ClientConfidential
     */
    public function setDateOfLanding($dateOfLanding)
    {
        $this->dateOfLanding = $dateOfLanding;

        return $this;
    }

    /**
     * Get dateOfLanding
     *
     * @return \DateTime 
     */
    public function getDateOfLanding()
    {
        return $this->dateOfLanding;
    }

    /**
     * Set citizenshipObtained
     *
     * @param \DateTime $citizenshipObtained
     * @return ClientConfidential
     */
    public function setCitizenshipObtained($citizenshipObtained)
    {
        $this->citizenshipObtained = $citizenshipObtained;

        return $this;
    }

    /**
     * Get citizenshipObtained
     *
     * @return \DateTime 
     */
    public function getCitizenshipObtained()
    {
        return $this->citizenshipObtained;
    }

    /**
     * Set immID
     *
     * @param string $immID
     * @return ClientConfidential
     */
    public function setImmID($immID)
    {
        $this->immID = $immID;

        return $this;
    }

    /**
     * Get immID
     *
     * @return string 
     */
    public function getImmID()
    {
        return $this->immID;
    }

    /**
     * Set fileNo
     *
     * @param string $fileNo
     * @return ClientConfidential
     */
    public function setFileNo($fileNo)
    {
        $this->fileNo = $fileNo;

        return $this;
    }

    /**
     * Get fileNo
     *
     * @return string 
     */
    public function getFileNo()
    {
        return $this->fileNo;
    }

    /**
     * Set refuCaseLawyer
     *
     * @param string $refuCaseLawyer
     * @return ClientConfidential
     */
    public function setRefuCaseLawyer($refuCaseLawyer)
    {
        $this->refuCaseLawyer = $refuCaseLawyer;

        return $this;
    }

    /**
     * Get refuCaseLawyer
     *
     * @return string 
     */
    public function getRefuCaseLawyer()
    {
        return $this->refuCaseLawyer;
    }

    /**
     * Set portOfEntry
     *
     * @param string $portOfEntry
     * @return ClientConfidential
     */
    public function setPortOfEntry($portOfEntry)
    {
        $this->portOfEntry = $portOfEntry;

        return $this;
    }

    /**
     * Get portOfEntry
     *
     * @return string 
     */
    public function getPortOfEntry()
    {
        return $this->portOfEntry;
    }

    /**
     * Set interpreterName
     *
     * @param string $interpreterName
     * @return ClientConfidential
     */
    public function setInterpreterName($interpreterName)
    {
        $this->interpreterName = $interpreterName;

        return $this;
    }

    /**
     * Get interpreterName
     *
     * @return string 
     */
    public function getInterpreterName()
    {
        return $this->interpreterName;
    }

    /**
     * Set referedBy
     *
     * @param string $referedBy
     * @return ClientConfidential
     */
    public function setReferedBy($referedBy)
    {
        $this->referedBy = $referedBy;

        return $this;
    }

    /**
     * Get referedBy
     *
     * @return string 
     */
    public function getReferedBy()
    {
        return $this->referedBy;
    }

    /**
     * Set homeTp
     *
     * @param string $homeTp
     * @return ClientConfidential
     */
    public function setHomeTp($homeTp)
    {
        $this->homeTp = $homeTp;

        return $this;
    }

    /**
     * Get homeTp
     *
     * @return string 
     */
    public function getHomeTp()
    {
        return $this->homeTp;
    }

    /**
     * Set workTp
     *
     * @param string $workTp
     * @return ClientConfidential
     */
    public function setWorkTp($workTp)
    {
        $this->workTp = $workTp;

        return $this;
    }

    /**
     * Get workTp
     *
     * @return string 
     */
    public function getWorkTp()
    {
        return $this->workTp;
    }

    /**
     * Set cellTp
     *
     * @param string $cellTp
     * @return ClientConfidential
     */
    public function setCellTp($cellTp)
    {
        $this->cellTp = $cellTp;

        return $this;
    }

    /**
     * Get cellTp
     *
     * @return string 
     */
    public function getCellTp()
    {
        return $this->cellTp;
    }

    /**
     * Set emer
     *
     * @param string $emer
     * @return ClientConfidential
     */
    public function setEmer($emer)
    {
        $this->emer = $emer;

        return $this;
    }

    /**
     * Get emer
     *
     * @return string 
     */
    public function getEmer()
    {
        return $this->emer;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return ClientConfidential
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments()
    {
        return $this->comments;
    }
}
