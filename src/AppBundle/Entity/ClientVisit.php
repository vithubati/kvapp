<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientVisit
 */
class ClientVisit
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $purposeOfVisit;

    /**
     * @var \DateTime
     */
    private $dateOfVisit;

    /**
     * @var string
     */
    private $comments;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set purposeOfVisit
     *
     * @param string $purposeOfVisit
     * @return ClientVisit
     */
    public function setPurposeOfVisit($purposeOfVisit)
    {
        $this->purposeOfVisit = $purposeOfVisit;

        return $this;
    }

    /**
     * Get purposeOfVisit
     *
     * @return string 
     */
    public function getPurposeOfVisit()
    {
        return $this->purposeOfVisit;
    }

    /**
     * Set dateOfVisit
     *
     * @param \DateTime $dateOfVisit
     * @return ClientVisit
     */
    public function setDateOfVisit($dateOfVisit)
    {
        $this->dateOfVisit = $dateOfVisit;

        return $this;
    }

    /**
     * Get dateOfVisit
     *
     * @return \DateTime 
     */
    public function getDateOfVisit()
    {
        return $this->dateOfVisit;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return ClientVisit
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments()
    {
        return $this->comments;
    }
}
