<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientPersonal
 */
class ClientPersonal
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $middleName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $gender;

    /**
     * @var \DateTime
     */
    private $dateOfBirth;

    /**
     * @var string
     */
    private $placeOfBirth;

    /**
     * @var string
     */
    private $email;

    /**
     * @var \AppBundle\Entity\Address
     */
    private $address;

    /**
     * @var \AppBundle\Entity\ClientConfidential
     */
    private $clientConfidential;

    /**
     * @var \AppBundle\Entity\ClientVisit
     */
    private $clientVisit;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return ClientPersonal
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set middleName
     *
     * @param string $middleName
     * @return ClientPersonal
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Get middleName
     *
     * @return string 
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return ClientPersonal
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return ClientPersonal
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set dateOfBirth
     *
     * @param \DateTime $dateOfBirth
     * @return ClientPersonal
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * Get dateOfBirth
     *
     * @return \DateTime 
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * Set placeOfBirth
     *
     * @param string $placeOfBirth
     * @return ClientPersonal
     */
    public function setPlaceOfBirth($placeOfBirth)
    {
        $this->placeOfBirth = $placeOfBirth;

        return $this;
    }

    /**
     * Get placeOfBirth
     *
     * @return string 
     */
    public function getPlaceOfBirth()
    {
        return $this->placeOfBirth;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return ClientPersonal
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set address
     *
     * @param \AppBundle\Entity\Address $address
     * @return ClientPersonal
     */
    public function setAddress(\AppBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \AppBundle\Entity\Address 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set clientConfidential
     *
     * @param \AppBundle\Entity\ClientConfidential $clientConfidential
     * @return ClientPersonal
     */
    public function setClientConfidential(\AppBundle\Entity\ClientConfidential $clientConfidential = null)
    {
        $this->clientConfidential = $clientConfidential;

        return $this;
    }

    /**
     * Get clientConfidential
     *
     * @return \AppBundle\Entity\ClientConfidential 
     */
    public function getClientConfidential()
    {
        return $this->clientConfidential;
    }

    /**
     * Set clientVisit
     *
     * @param \AppBundle\Entity\ClientVisit $clientVisit
     * @return ClientPersonal
     */
    public function setClientVisit(\AppBundle\Entity\ClientVisit $clientVisit = null)
    {
        $this->clientVisit = $clientVisit;

        return $this;
    }

    /**
     * Get clientVisit
     *
     * @return \AppBundle\Entity\ClientVisit 
     */
    public function getClientVisit()
    {
        return $this->clientVisit;
    }
}
